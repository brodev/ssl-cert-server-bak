package aws

import (
	"context"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
)

type Kms interface {
	EncryptWithContext(ctx context.Context, sess *session.Session, input *kms.EncryptInput) (*kms.EncryptOutput, error)
	DecryptWithContext(ctx context.Context, sess *session.Session, input *kms.DecryptInput) (*kms.DecryptOutput, error)
}

type KmsHandler struct{}

func NewKmsHandler() *KmsHandler {
	return &KmsHandler{}
}

func (k *KmsHandler) EncryptWithContext(ctx context.Context, sess *session.Session, input *kms.EncryptInput) (*kms.EncryptOutput, error) {
	srv := kms.New(sess)
	return srv.EncryptWithContext(ctx, input)
}

func (k *KmsHandler) DecryptWithContext(ctx context.Context, sess *session.Session, input *kms.DecryptInput) (*kms.DecryptOutput, error) {
	srv := kms.New(sess)
	return srv.DecryptWithContext(ctx, input)
}
