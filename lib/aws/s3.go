package aws

import (
	"bytes"
	"context"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/jxskiss/ssl-cert-server/utils"
)

type S3Bucket interface {
	GetClusterRegionName() string
	NewSession() *session.Session
	CheckObjectExist(ctx context.Context, sess *session.Session, key string) (bool, *s3.HeadObjectOutput)
	RemoveObjectIfExist(ctx context.Context, sess *session.Session, key string) (*s3.DeleteObjectOutput, error)
	UploadCertWithEncrypt(ctx context.Context, sess *session.Session, key string, body []byte) (*s3.PutObjectOutput, error)
}

type S3BucketHandler struct {
	ClusterRegion string
	Region        string
	Bucket        string
	AWSAccessKey  string
	AWSSecretKey  string
	KmsClient     Kms
}

func NewS3BucketHandler() (s *S3BucketHandler, err error) {
	s = &S3BucketHandler{
		ClusterRegion: utils.GetEnvValue("AWS_CLUSTER_REGION"),
		Region:        utils.GetEnvValue("AWS_REGION"),
		Bucket:        utils.GetEnvValue("AWS_BUCKET"),
		AWSAccessKey:  utils.GetEnvValue("AWS_ACCESS_KEY_ID"),
		AWSSecretKey:  utils.GetEnvValue("AWS_SECRET_ACCESS_KEY"),
	}

	if utils.IsEmptyString(s.ClusterRegion) || utils.IsEmptyString(s.Region) || utils.IsEmptyString(s.Bucket) || utils.IsEmptyString(s.AWSSecretKey) || utils.IsEmptyString(s.AWSAccessKey) {
		err = errors.New("s3 setting values not configured yet")
		return
	}
	return
}

func (s *S3BucketHandler) GetClusterRegionName() string {
	return s.ClusterRegion
}

func (s *S3BucketHandler) NewSession() *session.Session {
	return session.Must(session.NewSession(&aws.Config{
		Region:      aws.String(s.Region),
		Credentials: credentials.NewEnvCredentials(),
	}))
}

func (s *S3BucketHandler) CheckObjectExist(ctx context.Context, sess *session.Session, key string) (bool, *s3.HeadObjectOutput) {
	srv := s3.New(sess)
	output, err := srv.HeadObjectWithContext(ctx, &s3.HeadObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return false, nil
	}
	return true, output
}

func (s *S3BucketHandler) RemoveObjectIfExist(ctx context.Context, sess *session.Session, key string) (*s3.DeleteObjectOutput, error) {
	exist, _ := s.CheckObjectExist(ctx, sess, key)
	if !exist {
		return nil, errors.New("this object is not exist, delete action has stopped")
	}
	srv := s3.New(sess)
	return srv.DeleteObjectWithContext(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(s.Bucket),
		Key:    aws.String(key),
	})
}

func (s *S3BucketHandler) UploadCertWithEncrypt(ctx context.Context, sess *session.Session, key string, body []byte) (*s3.PutObjectOutput, error) {
	srv := s3.New(sess)
	input := &s3.PutObjectInput{
		Body:                 bytes.NewReader(body),
		Key:                  aws.String(key),
		Bucket:               aws.String(s.Bucket),
		ServerSideEncryption: aws.String("aws:kms"),
		SSEKMSKeyId:          aws.String(key),
	}
	return srv.PutObjectWithContext(ctx, input)
}
