package utils

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

func GetEnvValue(key string) string {
	return strings.TrimSpace(os.Getenv(key))
}

func IsEmptyString(value string) bool {
	return value == ""
}

func GenerateKey(date time.Time, region string, key string) string {
	return fmt.Sprintf("%d/%s/%s", date.Day(), region, key)
}

func GetFileFromPath(ctx context.Context, path string) ([]byte, error) {
	var (
		data []byte
		err  error
		done = make(chan struct{})
	)
	go func() {
		data, err = ioutil.ReadFile(path)
		close(done)
	}()
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case <-done:
	}
	if os.IsNotExist(err) {
		return nil, errors.New("file is not exist")
	}
	return data, nil
}
